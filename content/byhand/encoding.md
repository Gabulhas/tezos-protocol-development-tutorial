---
title: 'Encodings'
date: 2019-02-11T19:30:08+10:00
weight: 7
---


The `data_encoding` library is a module in the Tezos protocol that provides a way to serialize and deserialize data.

Serialization is the process of converting an object or data structure into a format that can be transmitted across a network or stored to a file.

Deserialization is the process of reconstructing the original object or data structure from the serialized format.

Serialization and deserialization are important for any communication between nodes in a network, as it enables different nodes to communicate in a standardized format. In the case of Tezos, the `data_encoding` library is used to encode all data that is communicated between nodes in the Tezos network, as well as data that is written to disk or placed in a block. It's also used for the communication between the shell and the protocol, since the data defined in the protocol is independent from the shell.

The `data_encoding` library provides a set of primitive encodings and a variety of combinators to encode more complex data structures. The primitive encodings include encodings for integers of different sizes, such as `Data_encoding.int8`, `Data_encoding.int64` or other commonly used types such as `Data_encoding.Z.t` for `Zarith`.

The library also provides encodings for lists, arrays, and options built on top of ground data types, and union types or record types.

In addition to serializing to binary format, the `data_encoding` library can also serialize data to JSON format or Binary.

Overall, the `data_encoding` library is a crucial component of the Tezos protocol, enabling standardized communication between nodes and efficient storage of data.


A more in depth tutorial of this library can be found here: [Data_encoding](https://gitlab.com/nomadic-labs/data-encoding/-/blob/master/src/tutorial.md).



## Examples

Here are some examples of the use of the `data_encoding` library (some taken from the Tezos documentation).


### Built-in encodings
```ocaml
let int31_encoding = Data_encoding.int31
```

In this example, we create an encoding for a 31-bit integer using the `Data_encoding.int31` primitive. This creates an encoding that maps to and from the `int` type in OCaml. We can use this encoding to serialize and deserialize 31-bit integers in a binary format suitable for communication within the Tezos node software and between nodes.

### Simple tuple encoding

```ocaml
type interval = int64 * int64

let interval_encoding =
  Data_encoding.(obj2 (req "min" int64) (req "max" int64))
```

In this example, we define a custom type `interval` that represents a pair of 64-bit integers. We then define an encoding for this type using the `Data_encoding.obj2` combinator, which creates an encoding for a two-field object. The `req` combinator indicates that both fields are required for encoding and decoding. The resulting encoding maps to a binary format that encodes the two `int64` fields in sequence.

### Lists, Arrays and Option types

```ocaml
type interval_list = interval list
type interval_array = interval array
type interval_option = interval option

let interval_list_encoding = Data_encoding.list interval_encoding
let interval_array_encoding = Data_encoding.array interval_encoding
let interval_option_encoding = Data_encoding.option interval_encoding
```

In these examples, we define three new types `interval_list`, `interval_array`, and `interval_option` that are based on the `interval` type we defined earlier. We then define encodings for each of these types using the `Data_encoding.list`, `Data_encoding.array`, and `Data_encoding.option` combinators, respectively.


### Union types

```ocaml
type variant = B of bool | S of string

let variant_encoding =
  let open Data_encoding in
  union ~tag_size:`Uint8
    [ case ~title:"B" (Tag 0) bool
        (function B b -> Some b | _ -> None)
        (fun b -> B b);
      case ~title:"S" (Tag 1) string
        (function S s -> Some s | _ -> None)
        (fun s -> S s) ]
```

In this example, we define a custom variant type `variant` that represents either a `bool` value or a `string` value. We then define an encoding for this type using the `Data_encoding.union` combinator, which creates an encoding for a union type based on several `case` definitions. Each `case` definition maps to one of the possible variants of the `variant` type. The `Tag` constructor specifies an integer tag for each variant, which is used to encode and decode the variant in the binary format. The `union` combinator creates an encoding that can handle all possible variants of the `variant` type.


### Record types and Compound encodings

```ocaml
type management_operation = {
  source : Account_repr.t;
  fee : Tez_repr.tez;
  counter : Z.t;
  content : management_operation_content;
}

let management_operation_encoding =
  let open Data_encoding in
  conv
    (fun {source; fee; counter; content} -> (source, fee, counter, content))
    (fun (source, fee, counter, content) -> {source; fee; counter; content})
    (obj4
       (req "source" Account_repr.encoding)
       (req "fee" Tez_repr.encoding)
       (req "counter" z)
       (req "content" management_operation_content_encoding))

```

In this example, we define a custom record type `management_operation` that represents a management operation on the Tezos blockchain. The record has four fields: `source`, `fee`, `counter`, and `content`. We then define an encoding for this type using the `Data_encoding.conv` combinator, which creates a compound encoding that maps the record to and from a tuple of its constituent fields.

The `req` combinator is used to indicate that all four fields are required for encoding and decoding. The `obj4` combinator creates an encoding for a four-field object, with each field's name specified as a string.

The `conv` combinator takes three arguments: a function to convert the record to a tuple, a function to convert a tuple back to the record, and the encoding to use for the tuple. In this case, we use the `obj4` encoding we defined earlier for the tuple.

We also define an encoding for the `management_operation_content` type used as a field in the `management_operation` record. This encoding is defined separately as `management_operation_content_encoding` and can be used in other encodings that require this type.

Compound encodings like this one are useful for encoding complex data structures that combine different types of data. They allow us to define a single encoding that can handle all the data in the structure, rather than having to define separate encodings for each individual field.
