---
title: 'Nakamoto consensus'
date: 2019-02-11T19:30:08+10:00
weight: 4
---

In this tutorial, we are implementing the Nakamoto Consensus protocol, which was originally implemented in Bitcoin, and is the first consensus algorithm of it's type.


The reason for choosing Nakamoto Consensus over the original Delegated Proof-of-Stake (DPoS) protocol is simply because of the fact that it's different from the original protocols implemented in Tezos and also because this protocol is the most popular and well understood.

The core idea behind Nakamoto Consensus is that nodes in the network compete to solve a complex mathematical puzzle (mining), known as the proof-of-work puzzle. This puzzle involves computing a nonce (a number) that, when hashed together with the block's contents, produces a hash value that satisfies a certain condition. This condition is set by the network and is adjusted dynamically to ensure that new blocks are added to the blockchain at a steady rate.

The first node to solve the proof-of-work puzzle broadcasts its solution to the network, along with the new block that it has created. Other nodes in the network then verify that the new block is valid and add it to their own copy of the blockchain. This process continues until the new block has been added to the majority of nodes' copies of the blockchain, at which point the block is considered confirmed.

The Nakamoto Consensus protocol has several desirable properties,  Fault Tolerance, and Termination, but it has Probabilistic Finality.

- **Fault Tolerance** makes it economically infeasible for a malicious node to alter the data in a block and catch up with the rest of the network.

- **Termination** means that every functioning node reaches a decision, even if it's not the final one.

- **Probabilistic Finality** means that all functioning nodes eventually agree on a single block, but the process is not deterministic and involves a certain degree of probability. Since the mining/creation of a block is done independently by participating nodes, there are times that 2 or more miners find a legitimate block. So the network has to decide on which block to use.

By implementing the Nakamoto Consensus protocol in this tutorial, we have created a secure and decentralized blockchain network that is capable of processing transactions in a reliable and efficient manner.

---

## Consensus Breakdown

The Nakamoto Consensus Algorithm is the core of the Bitcoin blockchain, and it is composed of several components that work together to make the network secure and decentralized. Here's a breakdown of its inner workings:

### Blockchain Structure
A blockchain is a decentralized, digital ledger that contains a record of all Bitcoin transactions. 
In other words, it's a data structure that is replicated between all the nodes of the network, meaning that each node has a copy of the network's blockchain state and updates it when it receives new blocks from the network.

In a blockchain, each block contains an header (that contains a reference to the previous block) and a section of data.

![Blockchain structure](blockchain-structure.svg)

In case of Bitcoin, each block contains 
- in the header: 
    - a timestamp
    - a cryptographic hash of the previous block 
    - a nonce
    - and more information: [check the orignal header](https://developer.bitcoin.org/reference/block_chain.html)
- in the data section:
    - a list of transactions

This forms a chain of blocks that cannot be altered without redoing the work of the entire chain, that is, in order to change a block, one has to change the hashes of all the children blocks, and that would require redoing all the work (hence the use of proof of work) and sharing those new blocks with the whole network.

### Proof of Work, Mining and Block Creation
Proof of Work (PoW) involves solving a mathematical puzzle to find a specific value, known as a hash, which is used to validate transactions and create new blocks. To find the hash, validators combine the result of the previous PoW round (the hash of the last block), data from transactions being validated, and a value called a nonce. 
Mining is the process of validating transactions and creating new blocks. Validators compete to solve the PoW puzzle, and the first one to do so gets to add the next block to the chain and receive a reward.

The proof of work aspect of this consensus solves two problems:
- Selection of random block creator
    - There's no deterministic way to select the new (temporary) leader
    - No one has full control of the network (decentralization)
    - No one knows who's the next creator of the block (no bribing)
- Ensuring that the chain is immutable in the whole network
    - In order to change the transactions of a Block, one has to recalculate the Proof of Work stamp (the nonce) of every block after it
    - One can say that, the older the block, the harder it is to change it (the more "immutable" it is)


In reality, mining is done has follows:
- A node receives transactions (and these are saved in a storage called "Mempool")
- When a node receives a new block from the network (including itself), the node:
    - creates a new block, containing the hash of the previous block, a nonce and a set of selected transactions from the Mempool (the transactions can be selected as the node wants)
    - it tries every single value for a nonce (every unsigned 32 bit integer), hashes the resulting header with every of those nonces until one of the hashes has the pretended characteristics
    - meanwhile, if it receives a block from the network, it stops mining that block and the process restarts
    - otherwise, it shares that block with the network


When a miner finds a block, it gets rewarded (in reality, it sends money to itself, in a special transaction). This way, miners are incentivized to mine and keep the network secure.
This also mints/creates new coins in the system, that has no coins to start with.

In case of Bitcoin, the pretended characteristics are:
- There's a value which is decided by the network (check [Difficulty and it's adjustment](#Difficulty-and-it's-adjustment)), that is called "Difficulty", and it changes during the execution of the system
- The hash of the header of the block must be small than the value of the difficulty (see how it's done in [Bitcoin](https://en.bitcoin.it/wiki/Difficulty))

It's possible to observe this in any Bitcoin Scanner/Explorer (where it's possible to check the blocks that are being shared in the network).

If you check [Blockchain.com](https://www.blockchain.com/en/explorer/blocks/btc/42), it's possible to observe that this block's header hash (and any other blocks headers') starts with multiple "0", and it's also possible to compare the values of the difficulty and hash, and the nonce that resulted in that hash.


### Block Selection Rules and Forks

Block selection is the process of selecting a validator to insert the next block in the chain. 
The first rule is the PoW race, where the validator who solves the puzzle first gets to add the block to the chain. The second rule is the Longest Chain Wins.

A (soft) fork occurs when two or more valid blocks are created at the same time, causing a split in the blockchain. Validators will choose which fork to work on based on the longest chain rule, which represents the most computational work done.
This concurrent creation happens because the mining process is done independently of the other nodes, and because the block gossip takes time to spread over the network.


### Gossip
Gossip is the process of propagating new blocks to other validators in the network. Validators broadcast newly mined blocks to their peers, who verify the PoW and add the block to their own local copy of the blockchain.
If a block is not valid, it's spread is stopped by the nodes.


### Difficulty and it's adjustment
The network adjusts the difficulty of the PoW puzzle every 2016 blocks (approximately every two weeks) to maintain a 10-minute block time. 
If blocks are being mined too quickly, the difficulty increases, and if they are being mined too slowly, the difficulty decreases.


### Reward Adjustment
Initially there's no single Bitcoin in the blockchain that can be traded. To fix this issue and to incentivize mining, the first node to find the hash (like explained in [[Proof of Work]](#ProofOfWork)), they create a transaction of a reward to themselves, that is included in the block.

The reward adjustment, also known as halving, is a mechanism in the Bitcoin protocol that reduces the reward given to miners for adding new blocks to the blockchain. It happens approximately every four years and the reward is cut in half.

This mechanism is designed to control the rate at which new Bitcoin is created and to ensure that there will only be 21 million Bitcoin in existence. As the reward for mining Bitcoin decreases, it becomes harder for miners to make a profit.


---
In this tutorial we will implement this consensus algorithm.

Here's a list of functionalities implemented in this tutorial:
- Simple peer-to-peer transactions (between two accounts)
- Difficulty adjustment
- Reward halving
- Reward the miner (Coinbase)
- Block validation, which includes:
    - Checking if transactions are valid (if one has enough money to transfer)
    - Checking if the block contains a valid hash, that is, if the hash of the block is lower than the difficulty expected by the network



