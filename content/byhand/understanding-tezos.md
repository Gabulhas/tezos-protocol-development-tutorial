---
title: 'Tezos Blockchain'
weight: 2
---


Tezos is a blockchain network launched in 2018 with the goal of creating a self-amending crypto-ledger that is secure, flexible, and upgradable. Unlike other blockchain networks that require hard forks to make changes to the protocol, Tezos' self-amendment capability allows for seamless upgrades without disrupting its community.

Tezos uses a Delegated Proof of Stake (DPoS) consensus mechanism where participants (or nodes) provide the necessary computational resources to keep the network running. However, Tezos' consensus is designed to be independent of other parts of the node, such as the Peer-to-Peer layer, the client, the validation layer, and the storage of the blockchain state on disk. This makes it easy to focus on writing the protocol and eliminates the need to worry about other components.

One of the key features of Tezos is its on-chain governance system, which allows all stakeholders to participate in governing the protocol. The election cycle provides a formal and systematic procedure for stakeholders to reach agreement on proposed protocol amendments. By combining this on-chain mechanism with self-amendment, Tezos can change its initial election process to adopt better governance mechanisms when they are discovered.

In addition to on-chain governance, Tezos incentivizes decentralized innovation by offering payment to individuals or groups that improve the protocol. This funding mechanism encourages robust participation and decentralizes the maintenance of the network. The development of an active, open, and diverse developer ecosystem that is incentivized to contribute to the protocol will facilitate Tezos development and adoption.

Tezos also offers a platform for creating smart contracts and building decentralized applications that cannot be censored or shut down by third parties. To improve security, Tezos facilitates formal verification, a technique used to mathematically prove properties about programs such as smart contracts. This technique can help avoid costly bugs and contentious debates that follow.

Lastly, Tezos uses a Proof-of-Stake (PoS) consensus mechanism that allows any stakeholder to participate in the consensus process and be rewarded by the protocol for contributing to the security and stability of the network. Tezos also provides a delegation option where users can delegate their rights to other users to participate on their behalf.

Overall, Tezos is a commercially well-tested and industry-grade blockchain network that is suitable for experimentation due to its upgradability and flexible architecture. Its on-chain governance system, decentralized innovation, smart contract capabilities, and PoS consensus mechanism make it an attractive option for building decentralized applications and exploring new consensus mechanisms.
