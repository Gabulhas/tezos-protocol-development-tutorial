---
title: 'Tezos Node Architecture'
weight: 3
---


Tezos has a unique structure that separates the "Economic Protocol" from the rest of the node, known as the shell. This architecture is depicted in the diagram below (this diagram was provided by [Tezos docs](https://tezos.gitlab.io/shell/the_big_picture.html)).

![Tezos Architecture Diagram](https://tezos.gitlab.io/_images/octopus.svg)

For simplicity's sake, we will only describe the Shell and the Protocol parts of the Tezos Architecture.

## The Shell

The shell includes the validation code, the peer-to-peer layer, the disk storage of blocks, and the versioned state of the ledger. It abstracts the fetching and replication of new chain data to the validator. The shell is responsible for handling everything except for interpreting transactions and administrative operations and detecting erroneous blocks, which is the responsibility of the protocol.

## The Economic Protocol

The Economic Protocol is stateless and only used to perform the logic part of the consensus. The protocol is responsible for interpreting transactions and administrative operations, detecting erroneous blocks, and describing what the shell should do when receiving new information. The communication between the shell and the protocol is based on the interfaces that will be explained later.

The Economic Protocol is subject to an amendment procedure, which allows for on-chain operations to switch from one protocol to another. A Tezos node can contain multiple economic protocols, but only one of them is activated at any given time.

This is the part of the architecture that we will focus on, since it's the swappable and the consensus algorithm is described in it.


Protocol activation in Tezos is a two-step process. Firstly, a command injects an "activation block" to the blockchain. This block contains the hash of the protocol to be used, and only one operation to activate it. The activation block is the only block using the genesis protocol, as this protocol doesn't contain any other functionality besides the activation of a different protocol. Secondly, the next block in the blockchain will be the first block using the activated protocol.


---

In summary, Tezos' unique architecture separates the Economic Protocol from the shell, which is responsible for everything except interpreting transactions and administrative operations and detecting erroneous blocks. The protocol is subject to an amendment procedure, allowing on-chain operations to switch from one protocol to another. 
