---
title: 'Creating Representations'
date: 2019-02-11T19:27:37+10:00
weight: 7
---

In the Tezos protocol, representations or definitions are used to define the different types of information that the protocol needs to function. 


Representations can be thought of as values, objects, or structs that are understood by the protocol. These values are stored in files that end with `*_repr.ml`.
The structure of these values is defined in these files, and their encoding and operations are also handled within them.

In this chapter, we will take a closer look at the different types of information that are represented in Tezos and how they are defined and manipulated within the protocol.

**Note**: This is not necessarily needed in order for the protocol to work. It's a convention used by the "Official" protocols implemented in Tezos. You only have to implement the functions required by the Environment Interface.

Most representation have the following definitions:
- Main type `t`, and inner types included in the main type.
- Encodings of the main type and inner types.
- Helper/Auxiliary functions that manipulate the types, like `to_string` functions or others.

For example, let's define the representation of a person, defined in the file `person_repr.ml`. Each person has a name, height, date of birth, etc

```ocaml
type date_of_birth = {day : int32; month : int32; year : int32}

let date_of_birth_encoding =
  let open Data_encoding in
  def "block_header.custom.encoding"
  @@ conv
       (fun {day; month; year} -> (day, month, year))
       (fun (day, month, year) -> {day; month; year})
       (obj3 (req "day" int32) (req "month" int32) (req "year" int32))

let date_of_birth_to_string d =
  Int32.to_string d.day ^ "/" ^ Int32.to_string d.month ^ "/"
  ^ Int32.to_string d.year ^ "/"

type person = {
  name : string;
  height : int32;
  weight : int32;
  date_of_birth : date_of_birth;
}

type t = person

let person_encoding =
  let open Data_encoding in
  def "block_header.custom.encoding"
  @@ conv
       (fun {name; height; weight; date_of_birth} ->
         (name, height, weight, date_of_birth))
       (fun (name, height, weight, date_of_birth) ->
         {name; height; weight; date_of_birth})
       (obj4
          (req "name" string)
          (req "height" int32)
          (req "weight" int32)
          (req "date_of_birth" date_of_birth_encoding))

let person_to_string p =
  "Name: " ^ p.name ^ " Height: " ^ Int32.to_string p.height ^ " Weight: "
  ^ Int32.to_string p.weight ^ " date_of_birth: "
  ^ date_of_birth_to_string p.date_of_birth

```

---
## Some representations in our protocol

Here are a few examples of representations defined in the Nakamoto Protocol.

---
### Target

In order to represent the concept of target in the protocol, that is, the value that has to be bigger than the hash of the header of the block, and that is periodically adjusted by the network.

This target must be included in the block header by the block creator.

This representation is defined in the file `target_repr.ml` and one way to represent the target can be the following one:

```ocaml
type t = Z.t

let zero = Z.of_string "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

let encoding = Data_encoding.z

let to_hex d = 
    let z_as_hex_string = Z.format "%x" d in
    let open Compare.Int in
    if String.length z_as_hex_string > 64 then
        raise (Failure "Z is out of range")
    else
        `Hex ( String.make (64 - String.length z_as_hex_string) '0' ^ z_as_hex_string)

let to_bytes d =
    d |> to_hex |> Hex.to_bytes


let adjust (target: t) (time_difference_ratio:int64) =
    (*
    time_difference_ratio = time_taken this epoch/ block_time * epoch_size
     *)
    Z.mul target (Z.of_int64 time_difference_ratio)

let to_hex_string d = 
    let as_hex = to_hex d  in
    match as_hex with 
    | `Hex a -> a

```

In our case, the type that defines the target can be the same type as Zarith's `t` type, and in this file we include the encoding of the type (once again, this is a straightforward example, where the encoding is the same as Zarith's) and where we include some helper functions (`to_hex`, `to_bytes`, `adjust` and `to_hex_string`).

### Header

In order to represent the Protocol's part of the block header, we define the contents of this part in the file `block_header_repr.ml`.

This is a "special" type of representation since we have to define some functions and types as they are required by the protocol environment we are using for this tutorial (v6).

The protocol part of the header contains the following information:
- `target` - Type `Target_repr.t`.
- `nonce` - Type `Int64.t` - this is the nonce that made the header have the desired proprpreties (hash value lower than the target).
- `miner` - The address of the miner that mined the block. Used to reward the miner.


```ocaml
type contents = {
  target : Target_repr.t;         
  nonce : Int64.t;      
  miner: Account_repr.t;
}

type protocol_data = contents

type t = {shell : Block_header.shell_header; protocol_data : contents}

type block_header = t

type raw = Block_header.t

type shell_header = Block_header.shell_header

let raw_encoding = Block_header.encoding

let shell_header_encoding = Block_header.shell_header_encoding

let contents_encoding =
  let open Data_encoding in
  def "block_header.custom.encoding"
  @@ conv
       (fun {target; nonce; miner} -> (target, nonce, miner))
       (fun (target, nonce, miner) -> {target; nonce; miner})
       (obj3
          (req "target" Data_encoding.z)
          (req "nonce" int64)
          (req "miner" Account_repr.encoding))

let protocol_data_encoding = contents_encoding

let raw {shell; protocol_data} =
  let protocol_data =
    Data_encoding.Binary.to_bytes_exn protocol_data_encoding protocol_data
  in
  {Block_header.shell; protocol_data}

let unsigned_encoding =
  let open Data_encoding in
  merge_objs Block_header.shell_header_encoding contents_encoding

let encoding =
  let open Data_encoding in
  def "block_header.custom.full_header"
  @@ conv
       (fun {shell; protocol_data} -> (shell, protocol_data))
       (fun (shell, protocol_data) -> {shell; protocol_data})
       (merge_objs Block_header.shell_header_encoding protocol_data_encoding)

(*...*)
```

As you can see, this is a special type of representation, since we have to define a few functions and some extra encodings. 

In reality, when blocks are exchanged in the network, the shell receives the block with a header containing the shell part and the protocol parts as Bytes, but they are decoded into this two parts.
This last one being represented in Bytes (even from the decoding side of the shell), and then is later decoded by the protocol using the `contents_encoding` encoding, that's why the variable `protocol_data` in the `raw` function is a Bytes.t variable, since the actual type of the Block header is the following `type t = {shell : shell_header; protocol_data : Bytes.t}`, as it's the structure "understood" by the shell.

---

## Other representations

There are many more representations included in the protocol. Here's the rest of the representations:
- **Accounts/Manager**: This is defined in the files `account_repr.ml` and `manager_repr.ml`. 
This represents the concept of an account in the protocol. An account is just a key, which is used to fetch information from the storage (like the current balance). It is just a Public Key Hash.
- **Constants/Parameters**: This is defined in the file `constants_repr.ml`. 
This file contains information that is constant throughout the execution of the protocol. Some constants can be defined when activating the protocol. The constants include:
  - `block_time`: Defines the time between blocks.
  - `initial_target`: Defines the first target value.
  - `difficulty_adjust_epoch_size`: Defines the number of blocks to wait to then readjust the target.
  - `halving_epoch_size`: Defines the number of blocks to wait to then halve the mining reward.
  - `reward_multiplier`: The initial reward, which then gets halved.
  - `Header`: Defines the Protocol header, as mentioned before.
  - `block_time`: Defines the time between blocks.
- **Time**: This is defined in the file `time_repr.ml`. Defines the type of time used throughout the protocol.
- **Tez**: This is defined in the file `tez_repr.ml` Defines the type and operations for the currency.
- **Operations**: This is defined in the file `operation_repr.ml`. Representations and functionalities of the available operations. These include Transactions (between two entities) and Reveal (maps a Public Key to a Public Key Hash, like the ones used in the Accounts).
